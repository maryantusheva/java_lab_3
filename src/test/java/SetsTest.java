import funcsets.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.function.Function;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SetsTest<T> {
    @Test
    public void EmptySetTest() {
        PurelyFunctionalSet <T> set1 = null;
        assertTrue(set1 == null);
    }
    @Test
    public void SingletoneSetTest() {
        PurelyFunctionalSet <String> set1 = PurelyFunctionalSet.singeltonSet("ha-ha");
        assertTrue(set1.contains("ha-ha"));
    }
    @Test
    public void ContainsSingletoneSetTest() {
        PurelyFunctionalSet <Integer> set1 = PurelyFunctionalSet.singeltonSet(4);
        assertTrue(set1.contains(4));
    }
    @Test
    public void UnionSetsTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > 0 && x < 3;
        PurelyFunctionalSet <Integer> set2 = (x) -> x > 10 && x <= 13;
        PurelyFunctionalSet <Integer> set3 = PurelyFunctionalSet.union(set1, set2);
        assertTrue(set3.contains(1));
        assertTrue(set3.contains(2));
        assertTrue(set3.contains(11));
        assertTrue(set3.contains(12) && set3.contains(13));
    }
    @Test
    public void IntersectionSetsTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > 9 && x < 100;
        PurelyFunctionalSet <Integer> set2 = (x) -> x > 11 && x <= 14;
        PurelyFunctionalSet <Integer> set3 = PurelyFunctionalSet.intersect(set1, set2);
        assertTrue(set3.contains(12));
        assertTrue(set3.contains(14) );
        assertTrue(set3.contains(13));
        assertTrue(!set3.contains(99));
    }

    @Test
    public void IntersectionSetsWithDifferentElementsTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > 9 && x < 100;
        PurelyFunctionalSet <Integer> set2 = (x) -> x > 1000 && x <= 2000;
        PurelyFunctionalSet <Integer> set3 = PurelyFunctionalSet.intersect(set1, set2);
        assertFalse(set3.contains(50));
        assertFalse(set3.contains(1040) );
    }

    @Test
    public void DiffSetsTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > 10 && x < 15;
        PurelyFunctionalSet <Integer> set2 = (x) -> x > 11 && x <= 13;
        PurelyFunctionalSet <Integer> set3 = PurelyFunctionalSet.diff(set1, set2);
        assertTrue(set3.contains(11));
        assertTrue(set3.contains(14));
        assertTrue(!set3.contains(12));
        assertTrue(!set3.contains(13));
    }

    @Test
    public void DiffDifferentSetsTest() {
        PurelyFunctionalSet <Integer> set1 = PurelyFunctionalSet.singeltonSet(25);
        PurelyFunctionalSet <Integer> set = PurelyFunctionalSet.diff(set1, set1);
        assertFalse(set.contains(25));
    }
    @Test
    public void FilterSetTest() {
        PurelyFunctionalSet <Integer> setToFilter = (x) -> x > -25 && x < 25;
        PurelyFunctionalSet <Integer> filtredSet = PurelyFunctionalSet.filter(setToFilter,
                (x)->x > 0 && x <10);
        assertTrue(filtredSet.contains(5));
        assertTrue(filtredSet.contains(7));
        assertTrue(filtredSet.contains(9));
        assertTrue(!filtredSet.contains(22));
    }

    @Test
    public void ForallTruePredicationTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > -81 && x < 81;
        boolean actual = SetOfIntegers.forall(set1, (x)->x > -81 && x<81);
        assertTrue(actual);
    }

    @Test
    public void ForallFalsePredicationTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > -81 && x < 81;
        boolean actual = SetOfIntegers.forall(set1, (x)->x > 0 && x<100);
        assertFalse(actual);
    }

    @Test
    public void ExistTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > 50 && x < 100;
        boolean actual = SetOfIntegers.exists(set1, (x)-> x > 50 && x< 60);
        assertTrue(actual);
    }

    @Test
    public void ExistForNoElementsTest() {
        PurelyFunctionalSet <Integer> set1 = (x) -> x > 0 && x < 100;
        boolean actual = SetOfIntegers.exists(set1, (x)-> x < 0 && x> 160);
        assertFalse(actual);
    }

    @Test
    public void MapTest() {
        PurelyFunctionalSet <Integer> set = (x) -> x > 0 && x < 6;
        Integer [] array = new Integer[]{1, 8, 27, 64, 125};
        PurelyFunctionalSet <Integer> set1 = SetOfIntegers.map(set, (x) ->x*x*x);
        for(int i : array){
            assertTrue(set1.contains(i));
        }
    }
}