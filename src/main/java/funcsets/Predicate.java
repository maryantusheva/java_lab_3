package funcsets;

public interface Predicate<T> {
    public boolean test(T Element);
}
