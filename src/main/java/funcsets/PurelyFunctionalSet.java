package funcsets;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

public interface PurelyFunctionalSet<T> {
    public boolean contains(T element);

    public static<T>PurelyFunctionalSet<T> empty() {
        return Objects::isNull;
    }

    public static <T> PurelyFunctionalSet<T> singeltonSet(T val) {
        return (x) -> x == val;
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> s,
                                                   PurelyFunctionalSet<T> t) {
        return (x) -> s.contains(x) || t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> s,
                                                       PurelyFunctionalSet<T> t) {
        return (x) -> s.contains(x) && t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> s,
                                                  PurelyFunctionalSet<T> t) {
        return (x) -> s.contains(x) && !t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> s,
                                                    Predicate<T> p) {
        return (x) -> p.test(x) && s.contains(x);
    }
}