package funcsets;

import java.util.function.Function;


public class SetOfIntegers {
    private static int bound = 1000;

    public static boolean forall(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return iterate(-bound, s, p);
    }
    private static boolean iterate(int el, PurelyFunctionalSet<Integer> set,
                                   Predicate<Integer> predicate){

        if (set.contains(el) && !predicate.test(el)) return false;
        if (el >= bound) return true;
        else return iterate(++el, set, predicate);
    }

    public static boolean exists(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return !forall(s, (x) -> !p.test(x));
    }

    public static <R> PurelyFunctionalSet<Integer> map(PurelyFunctionalSet<Integer> s,
                                                       Function<Integer, R> p) {
        return (y) -> exists(s, (x) -> p.apply(x) == y);
    }

}
