import funcsets.*;
import java.util.*;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import funcsets.*;

public class Main {
    public static void main(String[] args)
    {
        System.out.println(" Set #1: ");
        PurelyFunctionalSet <Integer> set1 = (x) -> x >-10 && x<10;
        System.out.println(" Function  x * 10");
        PurelyFunctionalSet <Integer> mapSet = SetOfIntegers.map(set1, (x) -> x * 10);
    }
}
